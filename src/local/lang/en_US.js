export default {
  bpmn: {
    actions: {
      import: "Import",
      importXml: "Import XML",
      importSvg: "Import SVG",
      preview: "Preview",
      clear: "Clear",
      save: "Save",
      close: "Close"
    }
  }
}